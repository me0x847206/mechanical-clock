/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.mechclock;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.sql.Time;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.awt.Color.BLACK;
import static java.awt.Color.BLUE;
import static java.awt.Color.RED;
import static java.awt.Color.WHITE;
import static java.awt.Color.YELLOW;
import static java.awt.Color.gray;
import static java.awt.Font.ITALIC;
import static java.awt.Font.PLAIN;
import static java.awt.event.KeyEvent.VK_F1;
import static java.lang.System.currentTimeMillis;

/**
 * @author Teodor MAMOLEA
 */
strictfp class MechanicalClockFrame extends Applet implements Runnable {

    private static final double CONST1 = 1.570796326795F;
    private static final double CONST2 = 0.10471975511966666F;

    private static final Color backColor = BLACK;
    private static final Color ovalColor = new Color(76, 76, 76);
    private static final Color secColor = YELLOW;
    private static final Color minColor = BLUE;
    private static final Color hourColor = RED;
    private static final Color dcColor = WHITE;

    private int d;
    private int i;
    private int xDC;
    private int yDC;

    private Point points[] = new Point[60];
    private Point _points[] = new Point[60];
    private Point __points[] = new Point[60];
    private Point ___points[] = new Point[60];
    private Point ____points[] = new Point[60];
    private Point center;

    private int sec;
    private long _sec;
    private int min;
    private int hour;

    private Graphics g;

    private boolean isFirstTime = true;
    private boolean showDigitalClock = false;

    MechanicalClockFrame(final int d) {
        super();

        this.d = d;

        setFont(new Font("font", ITALIC, 20));
        setBackground(backColor);

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent ke) {
                if (ke.getKeyCode() == VK_F1) {
                    setShowDigitalClock();
                }
            }
        });

        init();
    }

    @Override
    public void init() {
        final int y;
        final int x = y = 25;

        d -= 2 * x; // diametrul

        final int r = (int) (d * 0.5);
        final int _r = (int) (r * 0.2);
        final int __r = (int) (r * 0.4);
        final int ___r = (int) (r * 0.6);
        final int ____r = (int) (r * 0.8);

        center = new Point(x + r, y + r);
        xDC = center.x - 45;
        yDC = r + 33;

        double sin;
        double cos;

        for (int i = 0; i < 60; ++i) {
            sin = Math.sin(CONST1 - i * CONST2);
            cos = Math.cos(CONST1 - i * CONST2);

            points[i] = new Point(center.x + (int) (r * cos), center.y - (int) (r * sin));
            _points[i] = new Point(center.x + (int) (_r * cos), center.y - (int) (_r * sin));
            __points[i] = new Point(center.x + (int) (__r * cos), center.y - (int) (__r * sin));
            ___points[i] = new Point(center.x + (int) (___r * cos), center.y - (int) (___r * sin));
            ____points[i] = new Point(center.x + (int) (____r * cos), center.y - (int) (____r * sin));
        }
    }

    @Override
    public void paint(final Graphics g) {
        g.setColor(ovalColor);

        for (i = 0; i < 12; ++i) {
            int j;
            for (j = 1; j < 5; ++j) {
                g.drawOval(points[i * 5 + j].x - 3, points[i * 5 + j].y - 3, 6, 6);
            }
            g.drawOval(points[i * 5].x - 10, points[i * 5].y - 10, 20, 20);
        }

        if (isFirstTime) {
            String temp = "" + new Time(currentTimeMillis());
            min = Integer.parseInt(temp.substring(3, 5));
            hour = (Integer.parseInt(temp.substring(0, 2)) % 12) * 5 + (min / 12);
            setShowDigitalClock();
            new Thread(this).start();
            isFirstTime = false;
        }

        g.setColor(minColor);

        if (min % 5 == 0) {
            g.drawOval(points[min].x - 10, points[min].y - 10, 20, 20);
        } else {
            g.drawOval(points[min].x - 3, points[min].y - 3, 6, 6);
        }

        g.drawLine(__points[min].x, __points[min].y, ___points[min].x, ___points[min].y);
        g.setColor(hourColor);

        if (hour % 5 == 0) {
            g.drawOval(points[hour].x - 10, points[hour].y - 10, 20, 20);
        } else {
            g.drawOval(points[hour].x - 3, points[hour].y - 3, 6, 6);
        }

        g.drawLine(_points[hour].x, _points[hour].y, __points[hour].x, __points[hour].y);
        g.setColor(gray);
        g.setFont(new Font("font", PLAIN, 16));
        g.drawString("by me0x847206", 2 * center.x - 135, 2 * center.y - 4);
    }

    @Override
    public void run() {
        do {
            sec = (int) ((currentTimeMillis() / 1000)) % 60;
            try {
                _paint();
                Thread.sleep(1000 - (currentTimeMillis() - _sec));
            } catch (final InterruptedException ex) {
                Logger.getLogger(MechanicalClockFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }

    private void _paint() {
        _sec = currentTimeMillis();

        final Graphics g = getGraphics();
        g.setColor(backColor);

        if (sec == 0) {
            i = 59;

            g.drawLine(__points[min].x, __points[min].y, ___points[min].x, ___points[min].y);
            g.setColor(min == hour ? hourColor : ovalColor);

            if (min % 5 == 0) {
                g.drawOval(points[min].x - 10, points[min].y - 10, 20, 20);
            } else {
                g.drawOval(points[min].x - 3, points[min].y - 3, 6, 6);
            }

            min = min == 59 ? 0 : min + 1;
            g.setColor(minColor);

            if (min % 5 == 0) {
                g.drawOval(points[min].x - 10, points[min].y - 10, 20, 20);
            } else {
                g.drawOval(points[min].x - 3, points[min].y - 3, 6, 6);
            }

            g.setColor(backColor);

            if (min % 12 == 0) {
                g.drawLine(_points[hour].x, _points[hour].y, __points[hour].x, __points[hour].y);
                g.setColor(ovalColor);

                if (hour % 5 == 0) {
                    g.drawOval(points[hour].x - 10, points[hour].y - 10, 20, 20);
                } else {
                    g.drawOval(points[hour].x - 3, points[hour].y - 3, 6, 6);
                }

                if (hour == 59) {
                    if (showDigitalClock) {
                        setShowDigitalClock();
                        setShowDigitalClock();
                    }

                    hour = 0;
                } else {
                    ++hour;
                }

                g.setColor(hourColor);

                if (hour % 5 == 0) {
                    g.drawOval(points[hour].x - 10, points[hour].y - 10, 20, 20);
                } else {
                    g.drawOval(points[hour].x - 3, points[hour].y - 3, 6, 6);
                }

                g.setColor(backColor);
            }
        } else {
            i = sec - 1;
        }

        switch (sec % 5) {
            case 0:
                g.drawOval(points[sec].x - 10, points[sec].y - 10, 20, 20);
                g.setColor((i == min || i == hour ? (i == min) ? minColor : hourColor : ovalColor));
                g.drawOval(points[i].x - 3, points[i].y - 3, 6, 6);
                g.setColor(secColor);
                g.drawOval(points[sec].x - 15, points[sec].y - 15, 30, 30);
                break;
            case 1:
                g.drawOval(points[i].x - 15, points[i].y - 15, 30, 30);
                g.setColor((i == min || i == hour ? (i == min) ? minColor : hourColor : ovalColor));
                g.drawOval(points[i].x - 10, points[i].y - 10, 20, 20);
                g.setColor(secColor);
                g.drawOval(points[sec].x - 3, points[sec].y - 3, 6, 6);
                break;
            default:
                g.setColor((i == min || i == hour ? (i == min) ? minColor : hourColor : ovalColor));
                g.drawOval(points[i].x - 3, points[i].y - 3, 6, 6);
                g.setColor(secColor);
                g.drawOval(points[sec].x - 3, points[sec].y - 3, 6, 6);
                break;
        }

        g.setColor(backColor);
        g.drawLine(___points[i].x, ___points[i].y, ____points[i].x, ____points[i].y);

        if (showDigitalClock) {
            hideDC();

            g.setColor(dcColor);
            g.drawString("" + new Time(currentTimeMillis()), xDC, yDC);
        }

        if (min % 12 == 0 || i == hour) {
            g.setColor(hourColor);
            g.drawLine(_points[hour].x, _points[hour].y, __points[hour].x, __points[hour].y);
        }

        if (i == min || sec == 0) {
            g.setColor(minColor);
            g.drawLine(__points[min].x, __points[min].y, ___points[min].x, ___points[min].y);
        }

        g.setColor(secColor);
        g.drawLine(___points[sec].x, ___points[sec].y, ____points[sec].x, ____points[sec].y);
    }

    void setShowDigitalClock() {
        showDigitalClock = !showDigitalClock;

        if (!showDigitalClock) {
            hideDC(true);
        } else {
            showDate();
        }
    }

    private void hideDC() {
        g = getGraphics();
        g.setColor(backColor);
        g.fillRect(xDC, yDC - 17, 92, 17);
    }

    private void hideDC(final boolean _) {
        g = getGraphics();
        g.setColor(backColor);
        g.fillRect(xDC, yDC - 17, 92, 29);
    }

    private void showDate() {
        g = getGraphics();
        g.setFont(new Font("_font", PLAIN, 12));
        g.setColor(dcColor);
        g.drawString("" + new Date(currentTimeMillis()), xDC + 10, yDC + 12);
    }
}
