/*
 * Copyright (C) 2017 Teodor MAMOLEA <Teodor.Mamolea@gmail.com>
 *
 * ******************************************************************************
 *
 * DOWHATYOUWANTTODO
 *
 * ******************************************************************************
 */

package com.me0x.mechclock;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 * @author Teodor MAMOLEA
 */
public class Main {
    private static MechanicalClockFrame applet;

    public static void main(final String[] args) {
        final JFrame frame = new JFrame("Mechanical clock ( Press F1 for Show / Hide Digital Clock )");
        frame.getContentPane().setLayout(new BorderLayout());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        final int w = 700;
        frame.setSize(w, w + 70);
        frame.setLocation(300, 0);
        frame.setResizable(false);
        frame.getContentPane().add(applet = new MechanicalClockFrame(w));
        frame.setVisible(true);
        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(final KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_F1) {
                    applet.setShowDigitalClock();
                }
            }
        });
    }
}
